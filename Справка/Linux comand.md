Чекнуть интерфейсы:
```bash
ifconfig -a
ip link show
```

Чек переферии:
```bash
dmesg | grep tty
dmesg | grep spi
```

Запуск ноды canopen:
```bash
canopend can0 -i 1 -c "stdio"
```

Запуск виртуального can:
```bash
sudo modprobe vcan
sudo ip link add dev vcan0 type vcan
sudo ip link set up vcan0
```

Посмотреть каталог com портов:
```bash
ls -l /dev/ttyS* /dev/ttyUSB*
dmesg | grep "ttyS"
```

Чтение COM порта:
```bash
ttylog -d /dev/ttyS0
cat /dev/ttyS0
cat < /dev/ttyS0
```

#Справка
