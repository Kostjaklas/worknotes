# Что делает этот модуль?

Этот модуль добовляет дополнительные функции для стандартной библиотеки ARGB от Crazy Geeks

# Разбор кода

## Вспомогательные функции

### DELAY()

Этот участок кода определяет какую задержку мы будем использовать в блокирующих функциях анимаций

````c
#undef DELAY
    #ifdef USE_OS_DELAY
        #define DELAY osDelay
    #elif defined(USE_NOTIFY_DELAY)
        #define DELAY(x) DelayWithNotification(pdMS_TO_TICKS(x), xTaskGetCurrentTaskHandle())
    #elif defined(USE_NOTIFY_DELAY_BREAK)
        #define NOTIFY_DELAY(x) DelayWithNotification(pdMS_TO_TICKS(x), xTaskGetCurrentTaskHandle())
        #define DELAY(x) if (NOTIFY_DELAY(x)) break;
    #else
        #define DELAY HAL_Delay
#endif
````

Макрос `DELAY()` определяется в зависимости от флагов проекта и обеспечивает различные способы задержки выполнения кода. Поведение макроса меняется в зависимости от определенных условий компиляции, что позволяет гибко адаптироваться к разным средам выполнения.

**Варианты использования**
- `USE_FREERTOS`: Поддержка FreeRTOS.
- `USE_OS_DELAY`: Использует функцию `osDelay` из CMSIS.
- `USE_NOTIFY_DELAY`: Задержка FreeRTOS, которая прерывается по уведомлению (Использует функцию `DelayWithNotification` из FreeRTOS, которая может быть прервана)
- `USE_NOTIFY_DELAY_BREAK`: Аналогично предыдущему, но с выходом из цикла при получении уведомления.
- По умолчанию: Использует `HAL_Delay`, который основан на аппаратном таймере.

Для использования задержек `USE_OS_DELAY`, `NOTIFY_DELAY` и `USE_NOTIFY_DELAY_BREAK` убедитесь, что включена поддержка `USE_FREERTOS`.

### DelayWithNotification

Функция `DelayWithNotification` предназначена для использования в контексте FreeRTOS и реализует механизм задержки, который может быть прерван уведомлениями. Эта функция позволяет приостановить задачу на заданное количество тиков системного таймера. В то же время, задержка может быть прервана уведомлениями, посылаемыми другими задачами.

#### Пример кода
```c
uint32_t DelayWithNotification(uint32_t delayTicks, TaskHandle_t notifyingTask) {
    uint32_t ulNotificationValue;
    xTaskNotifyWait(0x00,          // Do not clear notification bits on entry.
                    0xFFFFFFFF,    // Clear all notification bits on exit.
                    &ulNotificationValue, // The notification value is passed here.
                    delayTicks);   // Delay in ticks

    return ulNotificationValue;
}
```

# Код библиотеки

## H файл

````c
/**
 * @file 	ARGB_Module.h
 * @author 	Konstantin Labuts
 * @date 	30-November-2023
 * @brief 	Module source file for the ARGB library by Crazy Geeks
 * @note	This module adds additional functions for the ARGB library
 */

#ifndef INC_ARGB_MODULE_H_
#define INC_ARGB_MODULE_H_

#include "ARGB.h"

// Configuration
#define USE_FREERTOS
//#define USE_NOTIFY_DELAY		// Use Interruptible delay (stop delay in animation)
#define USE_NOTIFY_DELAY_BREAK	// Use Interruptible delay with exit from the loop (stop animation)

#if defined(USE_FREERTOS)
    #include "FreeRTOS.h"
    #include "cmsis_os.h"
    #include "task.h"
#endif

void ARGB_FillRGB_Betwen(u16_t start, u16_t end, u8_t r, u8_t g, u8_t b);							 // Filling a specified strip range
void ARGB_FillThreeStates(uint8_t State, uint16_t startLen, uint16_t end_len);						 // Fill LED strip range with color based on state
void ARGB_RunningLine(int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms);					 // ARGB animation: RunningLine
void ARGB_Blink(int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, int BlinkCount); 			 // ARGB animation: Blink
void ARGB_Fade(u8_t start, u8_t end, u8_t r, u8_t g, u8_t b, u8_t delay_ms, u8_t currentBrightness); // ARGB animation: Fade

// Interruptible delay function to stop animation
#if defined(USE_FREERTOS)
uint32_t DelayWithNotification(uint32_t delayTicks, TaskHandle_t notifyingTask);
#endif

// The macro definition is in the .c file
#define DELAY()

typedef struct {
    uint32_t start_time;
    int current_step;
    int end_step;
    int is_running;
    int anim_step;
} AnimationState;

typedef struct {
	AnimationState animationState;
}argbModule_obj;

// Non-blocking function implementation
void ARGB_Blink_NONBLOCKING(argbModule_obj *argbModule, int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, int BlinkCount, int post_delay_ms);
void ARGB_RunningLine_NONBLOCKING(argbModule_obj *argbModule, int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, uint8_t SizieOfLine);
void ARGB_Fade_NONBLOCKING(argbModule_obj *argbModule, u8_t start, u8_t end, u8_t r, u8_t g, u8_t b, u8_t delay_ms, u8_t currentBrightness);

#endif

````

## C файл
````c
/**
 * @file 	ARGB_Module.c
 * @author 	Konstantin Labuts
 * @date 	30-November-2023
 * @brief 	Module source file for the ARGB library by Crazy Geeks
 * @note	This module adds additional functions for the ARGB library
 */

#include "ARGB_Module.h"

// Internal functions
uint32_t GetCurrentTime();
void Animation_End(AnimationState* state);
bool Animation_Update(AnimationState *state, uint32_t delay_ms, int step_change);
void Animation_Init(AnimationState *state, int current_step, int end_step);

/**
 * @brief DELAY()
 *
 * Defines the DELAY macro based on project flags
 *
 * Options:
 * - USE_FREERTOS: Enable support for FreeRTOS.
 * - USE_OS_DELAY: Uses osDelay from CMSIS.
 * - USE_NOTIFY_DELAY: Uses DelayWithNotification from FreeRTOS (interruptible).
 * - USE_NOTIFY_DELAY_BREAK: Like above, but exits enclosing loop on notification.
 * - Default: Uses HAL_Delay (hardware timer-based delay).
 *
 * Note: For USE_OS_DELAY, NOTIFY_DELAY & USE_NOTIFY_DELAY_BREAK ensure USE_FREERTOS is enabled.
 */
#undef DELAY
    #ifdef USE_OS_DELAY
        #define DELAY osDelay
    #elif defined(USE_NOTIFY_DELAY)
        #define DELAY(x) DelayWithNotification(pdMS_TO_TICKS(x), xTaskGetCurrentTaskHandle())
    #elif defined(USE_NOTIFY_DELAY_BREAK)
        #define NOTIFY_DELAY(x) DelayWithNotification(pdMS_TO_TICKS(x), xTaskGetCurrentTaskHandle())
        #define DELAY(x) if (NOTIFY_DELAY(x)) break;
    #else
        #define DELAY HAL_Delay
#endif


/**
 * @brief ARGB func: Filling a specified strip range.
 *
 * @param start The starting value of the range
 * @param end The ending value of the range
 * @param r The red color component (0-255)
 * @param g The green color component (0-255)
 * @param b The blue color component (0-255)
 */
void ARGB_FillRGB_Betwen(u16_t start, u16_t end, u8_t r, u8_t g, u8_t b)
{
	for (u16_t i = start; i<= end; i++)
	{
		ARGB_SetRGB(i, r, g, b);
	}
}

/**
 * @brief ARGB func: Fill three states
 *
 * Fill LED strip range with color based on state
 *
 * @param State The state for color selection. 0 for white (No), 1 for yellow (Yes), 2 for red (Extreme).
 * @param startLen The starting index in the LED strip range.
 * @param end_len The ending index in the LED strip range.
 */
void ARGB_FillThreeStates(uint8_t State, uint16_t startLen, uint16_t end_len)
{
	if(State == 0) // No
	{
		ARGB_FillRGB_Betwen(startLen, end_len, 255,255,255);
	}
	if(State == 1) // Yes
	{
		ARGB_FillRGB_Betwen(startLen, end_len, 255,255,0);
	}
	if(State == 2) // Extreme
	{
		ARGB_FillRGB_Betwen(startLen, end_len, 255,0,0);
	}
	while (ARGB_Show() != ARGB_OK);
}

/**
 * @brief ARGB Animation: Running line
 *
 * Displays a running light line from the start to the end position with the color (r, g, b).
 *
 * @param start The starting index in the range of LEDs where the effect begins.
 * @param end The ending index in the range of LEDs where the effect ends.
 * @param r The red color component (0-255).
 * @param g The green color component (0-255).
 * @param b The blue color component (0-255).
 * @param delay_ms The delay in milliseconds between turning each LED on and off.
 */
void ARGB_RunningLine(int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms)
{
	ARGB_FillRGB(r, g, b);
	for (int i = start; i <= end; i++)
	{
		ARGB_SetRGB(i, r, g, b);
		while (ARGB_Show() != ARGB_OK);

		DELAY(delay_ms);

		ARGB_SetRGB(i, 0, 0, 0);
	}
}

/**
 * @brief ARGB Animation: Blink
 *
 * Flashes a section of an ARGB LED strip between specified indices with a given color.
 *
 * @param start The starting index of the LEDs to blink (inclusive)
 * @param end The ending index of the LEDs to blink (inclusive).
 * @param r Red component of the color (0-255).
 * @param g Green component of the color (0-255).
 * @param b Blue component of the color (0-255).
 * @param delay_ms The delay in milliseconds between on and off switching of the LEDs.
 * @param BlinkCount The number of times to blink.
 */
void ARGB_Blink(int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, int BlinkCount)
{
	for (int i = 0; i <= BlinkCount; i++)
	{
		ARGB_Clear();
		while (ARGB_Show() != ARGB_OK);
		DELAY(delay_ms);
		ARGB_FillRGB_Betwen(start, end, r, g, b);
		while (ARGB_Show() != ARGB_OK);
		DELAY(delay_ms);
	}
}

/**
 * @brief ARGB Animation: Fade
 *
 * Gradually changes the brightness of a specified section of an ARGB LED strip to create a fade effect.
 *
 * @param start The starting index of the LEDs for the fade effect.
 * @param end The ending index of the LEDs for the fade effect.
 * @param r Red component of the color (0-255).
 * @param g Green component of the color (0-255).
 * @param b Blue component of the color (0-255).
 * @param delay_ms The delay in milliseconds between each step of the fade effect.
 * @param currentBrightness The initial brightness level from which the fading starts and to which it returns. Range is typically from 0 (off) to 255 (maximum brightness).
 */
void ARGB_Fade(u8_t start, u8_t end, u8_t r, u8_t g, u8_t b, u8_t delay_ms, u8_t currentBrightness)
{
	int brightness = currentBrightness;
	for(; brightness >= 0; brightness--)
	{
		ARGB_SetBrightness(brightness);
		ARGB_FillRGB_Betwen(start, end, r, g, b);
		while (ARGB_Show() != ARGB_OK);
		DELAY(delay_ms);
	}brightness++;
	for(; brightness <= currentBrightness; brightness++)
	{
		ARGB_SetBrightness(brightness);
		ARGB_FillRGB_Betwen(start, end, r, g, b);
		while (ARGB_Show() != ARGB_OK);
		DELAY(delay_ms);
	}
}

/**
 * @brief Non-blocking ARGB Animation: Blink
 *
 * Initiates and controls a non-blocking blinking animation on a section of an ARGB LED strip.
 * The specified LED segment blinks with a given color for a defined number of times.
 * This function should be called repeatedly in a loop to update the animation state.
 *
 * @param start The starting index of the LEDs to blink (inclusive)
 * @param end The ending index of the LEDs to blink (inclusive).
 * @param r Red component of the color (0-255).
 * @param g Green component of the color (0-255).
 * @param b Blue component of the color (0-255).
 * @param delay_ms The delay in milliseconds between on and off switching of the LEDs.
 * @param BlinkCount The number of times to blink.
 */
void ARGB_Blink_NONBLOCKING(argbModule_obj *argbModule, int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, int BlinkCount, int post_delay_ms)
{
	AnimationState *state = &argbModule->animationState;

	if (!state->is_running)
	{
		// Multiply by 2, because blinking involves two steps: on and off.
		if(state->anim_step == 0) Animation_Init(state , 0, BlinkCount * 2 - 1);
		else if (state->anim_step == 1) Animation_Init(state , 0, 1);
	}

	 if(state->anim_step == 0)
	 {
		 if(Animation_Update(state, delay_ms, 1))
		 {
			 if(!(state->current_step % 2 == 0))
			 {
				 ARGB_FillRGB_Betwen(start, end, r, g, b);
			 }
			 else
			 {
				 ARGB_Clear();
			 }
			 while (ARGB_Show() != ARGB_OK);
		 }
	 }
	 if(state->anim_step == 1)
	 {
		 if(Animation_Update(state, post_delay_ms, 1)) {}
	 }

	 if (!state->is_running)
	 {
		 if(post_delay_ms != 0)
		 {
			 if(state->anim_step == 0) state->anim_step = 1;
			 else if (state->anim_step == 1) state->anim_step = 0;
		 }
	 }
}

/**
 * @brief Non-blocking ARGB Animation: Fade
 *
 * Initiates and controls a non-blocking fade animation on a section of an ARGB LED strip.
 * The LEDs in the specified range fade in and out with the given color.
 * This function should be called repeatedly in a loop to update the animation state.
 *
 * @param start The starting index of the LEDs for the fade effect.
 * @param end The ending index of the LEDs for the fade effect.
 * @param r Red component of the color (0-255).
 * @param g Green component of the color (0-255).
 * @param b Blue component of the color (0-255).
 * @param delay_ms The delay in milliseconds between each step of the fade effect.
 * @param currentBrightness The initial brightness level from which the fading starts and to which it returns. Range is typically from 0 (off) to 255 (maximum brightness).
 */
void ARGB_Fade_NONBLOCKING(argbModule_obj *argbModule, u8_t start, u8_t end, u8_t r, u8_t g, u8_t b, u8_t delay_ms, u8_t currentBrightness)
{
	AnimationState *state = &argbModule->animationState;

	if (!state->is_running)
	{
		Animation_Init(state, currentBrightness, 0);
	}

	// Determining which direction we should move
	int step_change = (state->current_step > state->end_step) ? -1 : 1;

	if (Animation_Update(state, delay_ms, step_change))
	{
		ARGB_SetBrightness(state->current_step);
		ARGB_FillRGB_Betwen(start, end, r, g, b);
		while (ARGB_Show() != ARGB_OK);

		ARGB_SetBrightness(currentBrightness);

		// Switch animation direction if one of the ends is reached
		if (state->current_step == 0 || state->current_step == currentBrightness)
		{
			 state->end_step = (state->end_step == 0) ? currentBrightness : 0;
			 Animation_Init(state, state->current_step, state->end_step);
		}
	}
}

/**
 * @brief Non-blocking ARGB Animation: Running Line
 *
 * Initiates and controls a non-blocking running line animation on a section of an ARGB LED strip.
 * A single LED moves from the start to the end position, creating the effect of a running light.
 * This function should be called repeatedly in a loop to update the animation state.
 *
 * @param start The starting index in the range of LEDs where the effect begins.
 * @param end The ending index in the range of LEDs where the effect ends.
 * @param r The red color component (0-255).
 * @param g The green color component (0-255).
 * @param b The blue color component (0-255).
 * @param delay_ms The delay in milliseconds between turning each LED on and off.
 */
void ARGB_RunningLine_NONBLOCKING(argbModule_obj *argbModule, int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, uint8_t SizieOfLine)
{
	AnimationState *state = &argbModule->animationState;

	if(!state->is_running)
	{
		Animation_Init(state, start, end);
		state->current_step = start-1;
	}

	if(Animation_Update(state, delay_ms, 1))
	{
		ARGB_SetRGB(state->current_step, r, g, b);
		if(state->current_step != start)
		{
			ARGB_SetRGB(state->current_step - SizieOfLine, 0, 0, 0);
		}
		while (ARGB_Show() != ARGB_OK);
	}

	if (state->current_step > state->end_step)
	{
		Animation_End(state);
	}
}


/**
 * @brief Initialize the animation state.
 *
 * This function initializes the animation state by setting initial parameters
 * and marking the start of an animation. It should be called before starting
 * each new animation.
 *
 * @param state Pointer to the AnimationState structure holding the animation state.
 * @param current_step The initial step of the animation.
 * @param end_step The final step of the animation.
 */
void Animation_Init(AnimationState *state, int current_step, int end_step)
{
    if (state->is_running == 0)
    {
        state->start_time = GetCurrentTime();
        state->current_step = current_step;
        state->end_step = end_step;
        state->is_running = 1;
        ARGB_Clear();
        while (ARGB_Show() != ARGB_OK);
    }
}

/**
 * @brief Update the animation state.
 *
 * This function should be called on each update cycle to check if it's time to
 * update the animation step. It accounts for the delay and changes the animation
 * step according to the specified change. Returns true if the animation step was updated.
 *
 * @param state Pointer to the AnimationState structure containing the current state of the animation.
 * @param delay_ms The delay in milliseconds between animation step updates.
 * @param step_change The amount by which to change the animation step. Can be positive, negative, or zero.
 * @return Returns true if the animation step was updated, otherwise false.
 */
bool Animation_Update(AnimationState *state, uint32_t delay_ms, int step_change)
{
    uint32_t current_time = GetCurrentTime();

    // Update animation step if enough time has passed
    if (state->is_running && (current_time - state->start_time >= delay_ms))
    {
        state->start_time = current_time;
        if (step_change != 0)
        {
        	if(step_change > 0) state->current_step += step_change;
        	if(step_change < 0) state->current_step -= step_change;

            // Change current_step according to the direction
            state->current_step += step_change;
            // Checking if we have crossed the border
            if ((step_change > 0 && state->current_step > state->end_step) ||
                (step_change < 0 && state->current_step < state->end_step))
            {
                // Adjusting the value
                state->current_step = state->end_step;
                Animation_End(state);
            }
        }
        return true;
    }
    return false;
}

/**
 * @brief Ends the current animation.
 *
 * This function stops the current animation by setting the is_running flag
 * in the AnimationState structure to false.
 *
 * @param state Pointer to the AnimationState structure holding the animation state.
 */
void Animation_End(AnimationState* state)
{
    state->is_running = false;
}

/**
 * @brief Retrieve the current time.
 *
 * This function returns the current system time. The time is platform-dependent
 * and can vary depending on the underlying system or OS.
 *
 * @return Returns the current system time in milliseconds.
 */
uint32_t GetCurrentTime()
{
    #ifdef USE_FREERTOS
    return xTaskGetTickCount();
    #else
    return HAL_GetTick();
    #endif
}

#if defined(USE_FREERTOS)
/**
 * @brief Interruptible delay function for use with FreeRTOS.
 *
 * This function implements an interruptible delay mechanism in FreeRTOS. It allows
 * a task to be suspended for a specified number of system timer ticks,
 * while also allowing the delay to be interrupted by notifications.
 *
 * @param delayTicks       Number of ticks to delay.
 * @param notifyingTask    Handle of the task to be notified to interrupt the delay.
 * @return                 Returns the notification value, which can be used
 *                         to determine the cause of the delay interruption.
 *
 * @note 				   If the function returns a non-zero value, it means that the delay was
 *       				   interrupted by a notification. A return value of zero indicates that the delay period elapsed as usual.
 */
uint32_t DelayWithNotification(uint32_t delayTicks, TaskHandle_t notifyingTask) {
    uint32_t ulNotificationValue;
    xTaskNotifyWait(0x00,          // Do not clear notification bits on entry.
                    0xFFFFFFFF,    // Clear all notification bits on exit.
                    &ulNotificationValue, // The notification value is passed here.
                    delayTicks);   // Delay in ticks

    return ulNotificationValue;
}
#endif

````


# Зарисовки
## Работаю над радугой

Добавить ``uint32_t buff[255];`` в ARGB_Module.c

````c
void ARGB_FillRGB_Betwen_HSV(u16_t start, u16_t end, u8_t hue, u8_t sat, u8_t val)
{
	for (u16_t i = start; i<= end; i++)
	{
		ARGB_SetHSV(i, hue, sat, val);
	}
}

void ARGB_InitBuffer()
{
	for(int i = 0; i <= 255; i++)
	{
		buff[i] = i;
	}
}

void ARGB_SetBufferHSV()
{
	for(int i = 0; i <= 255; i++)
	{
		ARGB_SetHSV(i, buff[i], 255, 200);
	}
}

void CycleBuffer()
{
	int lastCell = buff[255 - 1];

	for(int i = 255 - 1; i > 0; i--)
	{
		buff[i] = buff[i - 1];
	}
	buff[0] = lastCell;
}

void ARGB_FillRGB_Betwen_Graduate2(argbModule_obj *argbModule, int delay_ms, u16_t start, u16_t end)
{
	AnimationState *state = &argbModule->animationState;

	if (!state->is_running)
	{
		Animation_Init(state, start, end);
		ARGB_InitBuffer();
	}

	if (Animation_Update(state, delay_ms, 1))
	{
		CycleBuffer();
		ARGB_SetBufferHSV();
		while (ARGB_Show() != ARGB_OK);
	}
}
````

**Main для теста:**
````c
#include "proc_Main.h"
#include "CANOpenData.h"
#include "proc_CANopen.h"
#include "proc_LedMode.h"

#include "ARGB.h"
#include "ARGB_Module.h"

//#include "ColorPalete.h"

osThreadId MainProcHandler;
void StartMainProc(void const *arguments);

argbModule_obj argbModulee;

extern ARGBColor argbColor;

void MainProcInit()
{
	osThreadDef(MainProc, StartMainProc, osPriorityHigh, 0, 128);
	MainProcHandler = osThreadCreate(osThread(MainProc), NULL);
}

CANOpenData_t data;
void StartMainProc(void const *arguments)
{
	ARGB_Init();
	ARGB_Clear();
	ARGB_SetBrightness(STRIP_BRIGTNESS);
	for(;;)
	{
		//ARGB_FillRGB_Betwen_HSV(0, 100, 0, 255, 40);
		//ARGB_FillRGB_Betwen(0, 100, 255, 0, 0);
		//while (ARGB_Show() != ARGB_OK);

		//ARGB_FillRGB_Betwen_Graduate(0, 100, 100, 100);
		ARGB_FillRGB_Betwen_Graduate2(&argbModulee, 1, 0, 510);

		//const char *hexColor = "#eb3434";
		//ARGBColor argbColor = hexToARGB(hexColor);
		//ARGB_FillRGB(argbColor.red, argbColor.green, argbColor.blue);

		//ARGB_Show();
		osDelay(1);
	}
}

````

## Хексы шмексы

````c
#include "ColorPalete.h"

ARGBColor argbColor;

ARGBColor hexToARGB(const char *hex)
{
    ARGBColor color = {0, 0, 0, 0};

    if (hex[0] == '#' && (strlen(hex) == 7 || strlen(hex) == 9))
    {
        color.alpha = (unsigned char)strtol(hex + 1, NULL, 16) >> 24;
        color.red = (unsigned char)strtol(hex + 1, NULL, 16) >> 16;
        color.green = (unsigned char)strtol(hex + 1, NULL, 16) >> 8;
        color.blue = (unsigned char)strtol(hex + 1, NULL, 16);
    }

    argbColor = color;
    return color;
}

````

````c
#ifndef INC_COLOR_PALETE_H_
#define INC_COLOR_PALETE_H_

/* HSV hue range:
Red: 	 0 - 60   (300 - 360)
Yellow:	 60 - 120
Green: 	 120 - 180
Cyan:	 180 - 240
Blue:	 240 - 300
Magenta: 300 - 360
 */

// ARG color presers
#define COLOR_RED 		255,0,0
#define COLOR_GREEN 	0,255,0
#define COLOR_BLUE		0,0,255
#define COLOR_PURPLE	255,0,255
#define COLOR_YELLOW	255,255,0
#define	COLOR_CYAN		0,255,255

#include <stddef.h>

typedef struct {
    unsigned char alpha;
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} ARGBColor;

#define HEXtoARGB hexToARGB() argbColor.red, argbColor.green, argbColor.blue

ARGBColor hexToARGB(const char *hex);

#endif

````

## Fade c завершением
````c
void ARGB_Fade_NONBLOCKING(argbModule_obj *argbModule, u8_t start, u8_t end, u8_t r, u8_t g, u8_t b, 
		u8_t delay_ms,u8_t currentBrightness)
{
	AnimationState *state = &argbModule->animationState;

	if (!state->is_running)
	{
		if(state->anim_step == 0) 		Animation_Init(state, 0, currentBrightness);
		else if (state->anim_step == 1)	Animation_Init(state, currentBrightness, 0);
	}

	if(state->anim_step == 0)
	{
		if(Animation_Update(state, delay_ms, +1))
		{
			ARGB_SetBrightness(state->current_step);
			ARGB_FillRGB_Betwen(start, end, r, g, b);
			while (ARGB_Show() != ARGB_OK);

			ARGB_SetBrightness(currentBrightness);
		}
	}
	if(state->anim_step == 1)
	{
		if(Animation_Update(state, delay_ms, -1))
		{
			ARGB_SetBrightness(state->current_step);
			ARGB_FillRGB_Betwen(start, end, r, g, b);
			while (ARGB_Show() != ARGB_OK);

			ARGB_SetBrightness(currentBrightness);
		}
	}

	if(!state->is_running)
	{
		if(state->anim_step == 0) 	   state->anim_step = 1;
		else if(state->anim_step == 1) state->anim_step = 0;
	}
	
}
````

## Обработка уведомления для блокирующих функций
````c
// Send a notification to stop the animation (if use blocking func)
static uint8_t privMode;
if(privMode != OD_MODE)
{
	xTaskNotifyGive(LedModeProcHandle);
	privMode = OD_MODE;
}
````
#Библиотеки

