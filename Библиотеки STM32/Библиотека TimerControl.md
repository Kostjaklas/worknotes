# Код библиотеки

## TimerContol.h
````c
#ifndef INC_COLOR_PALETE_H_
#define INC_COLOR_PALETE_H_

#include "tim.h"

typedef struct
{
	TIM_HandleTypeDef *htim;
	uint32_t Counter;
} TimerControl;

void TimerControl_Init(TimerControl *this, TIM_HandleTypeDef *htim);
void TIM_Start(TimerControl *this);
uint32_t TIM_Get(TimerControl *this);
void TIM_Set(TimerControl *this, uint32_t value);
void TIM_Reset(TimerControl *this);
uint32_t TIM_GetCounter(TimerControl *this);
void TIM_SetCounter(TimerControl *this, uint32_t value);
void TIM_ResetCounter(TimerControl *this);


#endif

````

## TimerControl.c
````c
#include "TimerControl.h"

#define MAX_TIMERS 1
Timer_obj *registeredTimers[MAX_TIMERS];
int numberOfRegisteredTimers  = 0;

void Timer_Init(Timer_obj *this, TIM_HandleTypeDef *htim)
{
	this->htim = htim;
	this->Counter = 0;

	_RegisterTimer(this);
}

void Timer_Start(Timer_obj *this) {
	HAL_TIM_Base_Start(this->htim);
}

void Timer_Start_IT(Timer_obj *this) {
	HAL_TIM_Base_Start_IT(this->htim);
}

uint32_t Timer_Get(Timer_obj *this) {
	return __HAL_TIM_GET_COUNTER(this->htim);
}

void Timer_Set(Timer_obj *this, uint32_t value) {
	__HAL_TIM_SET_COUNTER(this->htim, value);
}

void Timer_Reset(Timer_obj *this) {
	__HAL_TIM_SET_COUNTER(this->htim, 0);
}

uint32_t Timer_GetCounter(Timer_obj *this) {
	return this->Counter;
}

void Timer_SetCounter(Timer_obj *this, uint32_t value) {
	this->Counter = value;
}

void Timer_ResetCounter(Timer_obj *this) {
	this->Counter = 0;
}

void _RegisterTimer(Timer_obj *timer)
{
	if (numberOfRegisteredTimers < MAX_TIMERS)
    {
    	registeredTimers[numberOfRegisteredTimers++] = timer;
    }
}

void TimerControl_HandleInterrupt(TIM_HandleTypeDef *htim) {
    for (int i = 0; i < numberOfRegisteredTimers; i++)
    {
        //if (htim->Instance == TIM3)
        if (registeredTimers[i]->htim == htim)
        {
            // Updating the counter
            registeredTimers[i]->Counter++;

            // Call the callback function, if it is set
            if (registeredTimers[i]->TimerCallback != NULL) {
                registeredTimers[i]->TimerCallback();
            }
        }
    }
}

````



#Библиотеки
