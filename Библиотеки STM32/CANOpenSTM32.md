# Протокол

Описание протокола CANOpen
http://microsin.net/programming/arm/canopen-overview.html

# Код

Пример драйвера:
https://github.com/polariss/STM32F103C8T6-CAN

CanOpenSTM32:
https://github.com/CANopenNode/CanOpenSTM32
https://github.com/viteo/STM32-BluePill-CANOpenNode

Видосик
https://www.youtube.com/watch?v=R-r5qIOTjOo


# Настройка PDO

## Настройка коммуникационных параметров

![[Pasted image 20231206104711.png]]
1. **Communication** - Индекс записи в словаре
2. **Mapping** - 
3. **COB** - Индификатор сообщения
4. **Type** - Тип PDO сообщения
	- `1 - 240` - **Синхронная передача** (Где число указывает на количество SYNC сообщений, после которого будет произведена передача)
	- `241 - 252` - **Режимы производителя** (Они могут быть использованы для выполнения пользовательских функций, которые не описаны в стандарте CANopen)
	- `253` - **Высокоприоритетное/Событийное PDO**
		- Это означает, что PDO будет отправляться как можно скорее, но после любого высокоприоритетного сообщения, такого как синхронное PDO или сообщения управления.
	- `255` - **Асинхронная передача** (По запросу или подписке)
	- `0` - Отключено
5. **Inhibit** - Время задержки в миллисекундах, которое должно пройти после отправки PDO, прежде чем может быть отправлено следующее PDO. Значение 0 указывает на то, что нет никакой задержки.

## Пример мапинга

Отправка данных
![[Pasted image 20231221081804.png]]

Прием данных
![[Pasted image 20231221081828.png]]

## Выбор COB-ID для RPDO и TPDO

- COB-ID для TPDO и RPDO в CANopen обычно формируется из функционального кода и узлового ID. Например, для TPDO начальное значение COB-ID может быть `0x180 + Node ID`, а для RPDO - `0x200 + Node ID`.

# Запуск RPDO Callback

## Конфиг
````c
#define CO_CONFIG_PDO ( CO_CONFIG_FLAG_CALLBACK_PRE | CO_CONFIG_RPDO_ENABLE | CO_CONFIG_RPDO_ENABLE | CO_CONFIG_TPDO_ENABLE | CO_CONFIG_GLOBAL_RT_FLAG_CALLBACK_PRE | CO_CONFIG_PDO_OD_IO_ACCESS )
````

Вставить в CO_driver_target.h

## Инициализация

````c
CO_RPDO_t *rpdo = &canopenNode.canOpenStack->RPDO[0];
CO_RPDO_initCallbackPre(rpdo, NULL, RPDO_Callback);
````



# Динамическое конфигурирование PDO

## Включение динамической кофигурации

При использовании `CO_CONFIG_FLAG_OD_DYNAMIC` в `CO_CONFIG_PDO`, PDO могут быть динамически настроены даже в оперативном состоянии NMT (Network Management). Это означает, что вы можете изменять конфигурацию PDO во время работы устройства, что предоставляет большую гибкость при управлении данными PDO.

## Подходы

### Использование API-функций `CO_PDOconfigMap()` и `CO_PDOconfigCom()`:

   - **Преимущества**: Стандартизированный подход, обеспечивающий согласованность с протоколом CANopen. Обеспечивает высокий уровень абстракции и упрощает управление PDO.
   - **Недостатки**: Может быть менее гибким в некоторых сценариях, требует полного переконфигурирования маппинга PDO при каждом изменении.

### Прямое изменение параметров PDO в `CO_OD_RAM`:
   - **Преимущества**: Большая гибкость и возможность изменять только необходимые параметры без полной переконфигурации PDO.
   - **Недостатки**: Более высокий риск ошибок из-за прямого вмешательства в сложные внутренние структуры данных. Требует глубокого понимания CANopenNode и может нарушить согласованность с протоколом CANopen.

## Примеры кода

### Использование API-функций CANopenNode

```c
#include "CANopen.h"

// Функция для динамической настройки PDO с использованием API функций CANopenNode
void configurePDOUsingAPI(uint16_t PDO_COB_ID, uint32_t *mappingArray, uint8_t noOfMappedObjects) {
    // Здесь мы настраиваем PDO, используя CO_PDOconfigMap() и CO_PDOconfigCom()
    CO_PDOconfigCom(CO->TPDO[0], PDO_COB_ID);
    CO_PDOconfigMap(CO->TPDO[0], mappingArray, noOfMappedObjects);
}
```

### Прямое изменение параметров PDO

```c
#include "CANopen.h"

// Функция для динамической настройки PDO путем прямого изменения CO_OD_RAM
void configurePDODirectly() {
    // Отключаем PDO
    CO_OD_RAM.PDO[0].communicationParameter.COB_IDUsedByTPDO |= 0x80000000;

    // Отключаем маппинг
    CO_OD_RAM.PDO[0].mappingParameter.numberOfMappedObjects = 0;

    // Настройка маппинга
    CO_OD_RAM.PDO[0].mappingParameter.mappedObject1 = 0x20000108;

    // Включаем маппинг с одним объектом
    CO_OD_RAM.PDO[0].mappingParameter.numberOfMappedObjects = 1;

    // Включаем PDO
    CO_OD_RAM.PDO[0].communicationParameter.COB_IDUsedByTPDO &= ~0x80000000;
}
```

## Конкретные примеры

### API-функции CANopenNode

Допустим, мы хотим настроить PDO для передачи двух объектов: один с индексом 0x2000 и подиндексом 0x01, другой с индексом 0x2002 и подиндексом 0x01.

```c
void configurePDOUsingAPI() {
    uint32_t mappingArray[] = {
        0x20000108, // Объект 1: Индекс 0x2000, Подиндекс 0x01, 8 бит
        0x20020110  // Объект 2: Индекс 0x2002, Подиндекс 0x01, 16 бит
    };
    uint8_t noOfMappedObjects = 2;

    // Конфигурируем TPDO1 с новым маппингом
    CO_PDOconfigMap(CO->TPDO[0], mappingArray, noOfMappedObjects);
}
```

### Прямое изменение параметров PDO

Предположим, у нас есть та же цель - настроить PDO для передачи тех же двух объектов.

```c
void configurePDODirectly() {
    // Отключаем PDO
    CO_OD_RAM.PDO[0].communicationParameter.COB_IDUsedByTPDO |= 0x80000000;

    // Отключаем маппинг
    CO_OD_RAM.PDO[0].mappingParameter.numberOfMappedObjects = 0;

    // Настройка маппинга
    CO_OD_RAM.PDO[0].mappingParameter.mappedObject1 = 0x20000108; // Объект 1
    CO_OD_RAM.PDO[0].mappingParameter.mappedObject2 = 0x20020110; // Объект 2

    // Включаем маппинг с двумя объектами
    CO_OD_RAM.PDO[0].mappingParameter.numberOfMappedObjects = 2;

    // Включаем PDO
    CO_OD_RAM.PDO[0].communicationParameter.COB_IDUsedByTPDO &= ~0x80000000;
}
```