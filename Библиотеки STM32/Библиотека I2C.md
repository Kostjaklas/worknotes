# Разбор кода

### Стартовые макросы
Если USE_I2C_INTERRUPT определено, то будут использоваться не блокирующие функции, иначе блокирующие.

````c
#ifdef USE_I2C_INTERRUPT
    #define I2C_TRANSMIT(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Transmit_IT(hi2c, DevAddress, pData, Size)
	#define I2C_RECEIVE(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Receive_IT(hi2c, DevAddress, pData, Size)
#else
    #define I2C_TRANSMIT(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Transmit(hi2c, DevAddress, pData, Size, 10)
	#define I2C_RECEIVE(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Receive(hi2c, DevAddress, pData, Size, 10)
#endif
````

### Функция i2c_Init
````c
void i2c_Init (i2c_obj* this, I2C_HandleTypeDef *hi2c)
{
	this -> hi2c = hi2c;
}
````

### Функция передачи
````c
void i2c_Transmit(i2c_obj *this, uint16_t DevAddress, uint8_t Data, uint16_t Size)
{
	this->pData = Data;
	I2C_TRANSMIT(this->hi2c, DevAddress << 1, &(this->pData), Size);
}
````

### Функция према
````c
void i2c_Receive(i2c_obj *this, uint16_t DevAddress, uint8_t Data, uint16_t Size)
{
	this->pData = Data;
	I2C_RECEIVE(this->hi2c, DevAddress << 1, &(this->pData), Size);
}
````

### Поочередное зажигание адресов
````c
void alternating_flashing(i2c_obj *this, uint16_t DevAddress)
{
	uint8_t data;
	for (uint8_t k = 0; k < 8; k++)
	{
		data = 255 - pow(2,k);
		HAL_I2C_Master_Transmit(this->hi2c, DevAddress << 1, &data, 1, 10);
		HAL_Delay (150);
	}
}

````

### Макросы адресов микрухи
````c
// Low_level
#define All_Pin_L 0b00000000
#define PA0_L 0b11111110
#define PA1_L 0b11111101
#define PA2_L 0b11111011
#define PA3_L 0b11110111
#define PA4_L 0b11101111
#define PA5_L 0b11011111
#define PA6_L 0b10111111
#define PA7_L 0b01111111

// High_level
#define All_Pin_H 0b11111111
#define PA0_H 0b00000001
#define PA1_H 0b00000010
#define PA2_H 0b00000100
#define PA3_H 0b00001000
#define PA4_H 0b00010000
#define PA5_H 0b00100000
#define PA6_H 0b01000000
#define PA7_H 0b10000000
````

# Весь код
### H файл
````c
#include "i2c.h"
#include "main.h"

typedef struct i2c
{
	I2C_HandleTypeDef *hi2c;
	uint8_t pData;
}i2c_obj;

// Low_level
#define All_Pin_L 0b00000000
#define PA0_L 0b11111110
#define PA1_L 0b11111101
#define PA2_L 0b11111011
#define PA3_L 0b11110111
#define PA4_L 0b11101111
#define PA5_L 0b11011111
#define PA6_L 0b10111111
#define PA7_L 0b01111111

// High_level
#define All_Pin_H 0b11111111
#define PA0_H 0b00000001
#define PA1_H 0b00000010
#define PA2_H 0b00000100
#define PA3_H 0b00001000
#define PA4_H 0b00010000
#define PA5_H 0b00100000
#define PA6_H 0b01000000
#define PA7_H 0b10000000

void i2c_Init (i2c_obj* this, I2C_HandleTypeDef *hi2c);
void i2c_Transmit(i2c_obj *this, uint16_t DevAddress, uint8_t Data, uint16_t Size);
void i2c_Receive(i2c_obj *this, uint16_t DevAddress, uint8_t Data, uint16_t Size);
void alternating_flashing (i2c_obj *this, uint16_t DevAddress);

````
### C файл

````c
#include "My_i2c.h"

//#define USE_I2C_INTERRUPT

#ifdef USE_I2C_INTERRUPT
    #define I2C_TRANSMIT(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Transmit_IT(hi2c, DevAddress, pData, Size)
	#define I2C_RECEIVE(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Receive_IT(hi2c, DevAddress, pData, Size)
#else
    #define I2C_TRANSMIT(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Transmit(hi2c, DevAddress, pData, Size, 10)
	#define I2C_RECEIVE(hi2c, DevAddress, pData, Size) HAL_I2C_Master_Receive(hi2c, DevAddress, pData, Size, 10)
#endif

void i2c_Init (i2c_obj* this, I2C_HandleTypeDef *hi2c)
{
	this -> hi2c = hi2c;
}

void i2c_Transmit(i2c_obj *this, uint16_t DevAddress, uint8_t Data, uint16_t Size)
{
	this->pData = Data;
	I2C_TRANSMIT(this->hi2c, DevAddress << 1, &(this->pData), Size);
}

void i2c_Receive(i2c_obj *this, uint16_t DevAddress, uint8_t Data, uint16_t Size)
{
	this->pData = Data;
	I2C_RECEIVE(this->hi2c, DevAddress << 1, &(this->pData), Size);
}

void alternating_flashing(i2c_obj *this, uint16_t DevAddress)
{
	uint8_t data;
	for (uint8_t k = 0; k < 8; k++)
	{
		data = 255 - pow(2,k);
		HAL_I2C_Master_Transmit(this->hi2c, DevAddress << 1, &data, 1, 10);
		HAL_Delay (150);
	}
}

````

#Библиотеки

