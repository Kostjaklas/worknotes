# Зарисовки кода
### Обработка уведомления для блокирующих функций
````c
// Send a notification to stop the animation (if use blocking func)
static uint8_t privMode;
if(privMode != OD_MODE)
{
	xTaskNotifyGive(LedModeProcHandle);
	privMode = OD_MODE;
}
````
### ARGB_API
###### Header
````c
/**
 * @file 	ARGB_API.h
 * @author 	Konstantin Labuts
 * @brief 	API for LED strip control
 */

#ifndef INC_ARGB_API_H_
#define INC_ARGB_API_H_

#include "ARGB_Module.h"
#include "ARGB.h"
//#incule "MyARGBLib.h"

// Parameters for API configuration
typedef enum { LibSingle, LibMultiple } LibSelection;
typedef enum { BlockingFunc, NonBlockingFucn } AnimationTypeSelection;
typedef enum { Strip1, Strip2, Strip3, Strip4 } StripSelection;

typedef struct
{
	LibSelection selectedLib;
	AnimationTypeSelection selectedAnimationType;
	StripSelection selectedStrip;

	void (*Init)(void* ARGB_API_t, uint16_t NumPixels, uint16_t TimNum, int8_t TimCH); // ARGB_API typedef
	void (*Clear)(void);
	void (*SetBrightness)(u8_t br);
	void (*SetRGB)(u16_t i, u8_t r, u8_t g, u8_t b);
	void (*SetHSV)(u16_t i, u8_t hue, u8_t sat, u8_t val);
	void (*SetWhite)(u16_t i, u8_t w);
	void (*FillRGB)(u8_t r, u8_t g, u8_t b);
	void (*FillHSV)(u8_t hue, u8_t sat, u8_t val);
	void (*FillWhite)(u8_t w);
	ARGB_STATE (*Ready)(void);
	ARGB_STATE (*Show)(void);

	void (*FillRGBBetwen)(u16_t start, u16_t end, u8_t r, u8_t g, u8_t b);
	void (*FillThreeStates)(uint8_t State, uint16_t startLen, uint16_t end_len);
	void (*AnimationRunningLine)(int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms);
	void (*AnimationBlink)(int start, int end, u8_t r, u8_t g, u8_t b, int delay_ms, int BlinkCount);
	void (*AnimationFade)(u8_t start, u8_t end, u8_t r, u8_t g, u8_t b, u8_t delay_ms, u8_t currentBrightness);

	//MyARGB MyARGB_t

}ARGB_API;

// Create an ARGB API object
ARGB_API* ARGBapi_Create(StripSelection slectedStrip, LibSelection selectedLib, AnimationTypeSelection selectedAnimationType);

// Initialization funcs for single and multiple LED strips
void LibSingleInit(void* ARGB_API_t, uint16_t NumPixels, uint16_t TimNum, int8_t  TimCH);
void LibMultipleInit(void* ARGB_API_t, uint16_t NumPixels, uint16_t TimNum, int8_t  TimCH);

#endif

````

###### Source
````c
/**
 * @file 	ARGB_API.c
 * @author 	Konstantin Labuts
 * @brief 	API for LED strip control
 */

#include "ARGB_API.h"

/**
 * @brief Creates a new instance of ARGB_API
 *
 * This function allocates memory for an ARGB_API structure and initializes it
 * based on the chosen parameters: type of LED strip, library, and animation type.
 *
 * @param selectedStrip The selected LED strip (Strip1, Strip2, Strip3, or Strip4).
 * @param selectedLib The chosen library (LibSingle or LibMultiple).
 * @param selectedAnimationType The chosen animation type (BlockingFunc or NonBlockingFunc).
 * @return Pointer to the created ARGB_API instance, or NULL if failed.
 */
ARGB_API* ARGBapi_Create(StripSelection slectedStrip, LibSelection selectedLib, AnimationTypeSelection selectedAnimationType)
{
	ARGB_API* this = malloc(sizeof(ARGB_API));
	if (!this) return NULL;

	this->selectedStrip = slectedStrip;
	this->selectedLib = selectedLib;
	this->selectedAnimationType = selectedAnimationType;

	switch(selectedLib)
	{
		case LibSingle:   this->Init = LibSingleInit;   break;
		case LibMultiple: this->Init = LibMultipleInit; break;
	}

	return this;
}

/**
 * @brief Initializes the library for working with a single LED strip.
 *
 * This function assigns functions for controlling LEDs using
 * the basic library for managing a single strip. It is called
 * from the `init` method of the `ARGB_API` structure based on
 * the selected library (`LibSingle`).
 *
 * @param ARGB_API_t Pointer to the ARGB_API structure.
 * @param NumPixels Number of pixels in the strip.
 * @param TimNum Timer number.
 * @param TimCH Timer channel.
 *
 * @note Due to the specifics of the library, the parameters `NumPixels`, `TimNum`,
 *       and `TimCH` should be configured in the header file of the library.
 */
void LibSingleInit(void* ARGB_API_t, uint16_t NumPixels, uint16_t TimNum, int8_t  TimCH)
{
	// Type casting
	ARGB_API* this = (ARGB_API*)ARGB_API_t;
	if (!this) return;

	// Init ARGB funcs
	this->Clear = ARGB_Clear;
	this->SetBrightness = ARGB_SetBrightness;
	this->SetRGB = ARGB_SetRGB;
	this->SetHSV = ARGB_SetHSV;
	this->SetWhite = ARGB_SetWhite;
	this->FillRGB = ARGB_FillRGB;
	this->FillHSV = ARGB_FillHSV;
	this->FillWhite = ARGB_FillWhite;
	this->Ready = ARGB_Ready;
	this->Show = ARGB_Show;

	// ARGB_Module funcs
	this->FillRGBBetwen = ARGB_FillRGB_Betwen;
	this->FillThreeStates = ARGB_FillThreeStates;
	this->AnimationRunningLine = ARGB_RunningLine;
	this->AnimationBlink = ARGB_Blink;
	this->AnimationFade = ARGB_Fade;

	// Calling the initialization function
	ARGB_Init();
}

/**
 * @brief Initializes the library for working with multiple LED strips.
 *
 * This function is intended for initializing control over multiple LED strips,
 * but it is currently not implemented. It is supposed to be called from the
 * `init` method of the `ARGB_API` structure when `LibMultiple` is selected
 * as the library.
 *
 * @param ARGB_API_t Pointer to the ARGB_API structure.
 * @param NumPixels Number of pixels in the strip.
 * @param TimNum Timer number.
 * @param TimCH Timer channel.
 */
void LibMultipleInit(void* ARGB_API_t,  uint16_t NumPixels, uint16_t TimNum, int8_t  TimCH)
{
/*
	// Type casting
	ARGB_API* this = (ARGB_API*)ARGB_API_t;
	if (!this) return;

	// Init funcs //
	this->Clear = this->MyARGB_t->Clear;
	this->SetBrightness = this->MyARGB_t->SetBrightness;
	this->SetRGB = this->MyARGB_t->SetRGB;
	this->SetHSV = this->MyARGB_t->SetHSV;
	this->SetWhite = this->MyARGB_t->SetWhite;
	this->FillRGB = this->MyARGB_t->FillRGB;
	this->FillHSV = this->MyARGB_t->FillHSV;
	this->FillWhite = this->MyARGB_t->FillWhite;
	this->ARGB_Ready = this->MyARGB_t->Ready;
	this->ARGB_Show = this->MyARGB_t->Show;

	// Calling the initialization function
	this->MyARGB_t->Init(InitData_t InitData);
	*/
}

````

# Sync_SetMsgType

``void Sync_SetMsgType(sync_obj *sync, mode_obj *mode);``
``void Sync_SetMsgType(sync_obj *sync, mode_obj *mode);``

````c
typedef struct
{
	bool_t event;
	uint32_t PeriodCounter;
	uint8_t MsgType;
} sync_obj;
````

````c
uint8_t LedModeProc_CheckSYNCMessType(void)
{
	return LedModeProcObject.sync.MsgType;
}
````

````c
void Sync_SetMsgType(sync_obj *sync, mode_obj *mode)
{
	switch(mode->Mode)
	{
		case Sleep:		sync->MsgType = TPDO_SYNCMessage_Simple; 		return;
		case Charging: 	sync->MsgType = TPDO_SYNCMessage_Simple; 		return;
		case Distance: 	sync->MsgType = TPDO_SYNCMessage_Distance; 		return;
		case Scale: 	sync->MsgType = TPDO_SYNCMessage_Scale; 		return;
		case Emergency: sync->MsgType = TPDO_SYNCMessage_Emergency; 	return;
		case Movement:	sync->MsgType = TPDO_SYNCMessage_MovementState; return;
		default:		sync->MsgType = TPDO_SYNCMessage_Simple; 		return;
	}
}
````

**Как выглядел main:**
````c
void StartMainProc(void const *arguments)
{
	for(;;)
	{
		LedModeProc_SetData(CANopenProc_GetDataP());
		if(LedModeProc_CheckSYNCEvent()) CANopenProc_SetODTimer(LedModeProc_GetTimerValue());
		if(LedModeProc_CheckSYNCEvent()) CANopenProc_TPDOsendRequest(LedModeProc_CheckSYNCMessType());

		osDelay(1);
	}
}

````


# Сброс таймера на мастере
````c
// Reset timer
bool_t isAnimationMode = (LedModeProcObject.mode.isAnimationMode);
bool_t AnimationIsEnd = (LedModeProcObject.mode.argbModule.AnimationState.is_running);
if (Timer_GetCounter(&Timer) > 10000 && ((isAnimationMode && AnimationIsEnd) || !isAnimationMode))
{
	Timer_ResetCounter(&Timer);
}
````



#ARGB