#### Варианты решения проблеммы:

##### 1. Настроить режим сравнения таймера, чтобы генерить частоту

Проблемма данного способа решения проблеммы в реализации:
- Как с помощью Output Compare дергать пин, не создавая длительность?
- Проблемма в математике (регистры Output Compare режима всего 16 и 32 бит, не хватит памяти под прасчет частоты таймера)

##### 2. Вручную дергать через Calback пины

В обработке прерываний сверять текущее значение счетчика таймера, если текущее значение счетчика таймера равно заданной частоте канала, дергаем пин.

##### 3. Использовать разные таймера для каждого частотного выхода

Минус данного способа в том что не все частотные выходы будут задействованы, но данный способ самый быстрый для решения проблемы

#### Вывод

Для решения проблемы частотных выходов выберем третий способ. На данный момент нам необходимо только два частотных выхода, и приоритетом является быстрое решение проблемы.

#FFBB

