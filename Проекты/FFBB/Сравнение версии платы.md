# FFBB

## Схемы
Версия 1:
[[FFBBv1.PDF]]
[[../Files/FFBBv1/FFBBv1.PrjPcb|FFBBv1.proj]]
Версия 2:
[[FFBBv2.PDF]]

**Проверка отличия одной схемы от другой:**
- [x] Дискретные входы
- [x] Дискретные выхода
- [ ] LED часть
v1:
![[Pasted image 20231122112515.png]]
v2:
![[Pasted image 20231122112618.png]]
- [x] ISO часть
- [x] rs232
- [ ] Питание
v1:
![[ffbb1.png|Pasted image 20231124104421]]
v2:
![[ffbb2.png|Pasted image 20231124104439]]
- [ ] Токовая петля
v1:
![[ffbb3.png|Pasted image 20231124104858]]
v2:
![[ffbb4.png|Pasted image 20231124104923]]
Reciever:
![[ffbb5.png|Pasted image 20231124105139]]
Transmiter:
![[ffbb6.png|Pasted image 20231124105215]]


## КД робота
[[RS232 R-30iB Plus.pdf]]
[[RS232 R-30iB Mate  Plus.pdf]]

Разъем от робота - https://download.datasheets.com/pdfs/2006/06/14/pemco_a/manual/hk/pitch%20connectors/pcr-e(%20)fs_4e.pdf

## ТТ
[[ТТ FFBB.pdf]]
[[ТТ FFBB V2.pdf]]

## Внедрение платы
[[INDUSTRIAL-Внедрение платы маркиратора в Витеском офисе ЛАЦИТ 1-2 ноября 2023-201123-125138.pdf]]

## Индикация
Индикация осуществляется с помощью микросхемы [[PCF8574T]].

 #FFBB

