### Настройка автоматического входа под пользователем root в Ubuntu Server

1. Создайте каталог для конфигурации юнита службы systemd, если он еще не существует:
```bash
sudo mkdir -p /etc/systemd/system/getty@tty1.service.d/
```

2. Создайте файл юнита службы systemd для настройки автоматического входа в систему под пользователем root:
```bash
sudo nano /etc/systemd/system/getty@tty1.service.d/autologin.conf
```

3. В открывшемся редакторе добавьте следующие строки:
```bash
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin root --noclear %I $TERM
```

4. Обновите конфигурацию systemd, чтобы применить изменения:
```bash
sudo systemctl daemon-reload
```


## Как добавить скрипт Python в автозапуск на Ubuntu Server

1. Откройте файл `.bashrc` или `.bash_profile`:
```bash
nano ~/.bashrc
```

2.Добавьте строку для запуска вашего скрипта:
```bash
python3 /путь/к/вашему/скрипту.py
```